# Resume Generator

Forked From: https://www.overleaf.com/articles/ismail-shaikhs-business-cv/hwqzkdzjtpcb


Build with:
docker run -v $(pwd):/app $(docker build . -q)


Running Locally:
if you have `entr` and zathura(with pdf extentions) installed, you can run
`./util/recompile_on_changes.sh`

to keep a refreshed copy open

Note: https://gitlab.com/Jimmyscene/resume/-/jobs/artifacts/master/file/resume.pdf?job=build_resume
is the url to get the latest resume
